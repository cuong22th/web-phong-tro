package com.mock.payload.request;

import com.mock.entity.Comment;
import com.mock.entity.RoomStatus;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class PostDto {
    private Integer postId;
    private String title;
    private String content;
    private String roomImage;
    private String roomAddress;
    private double roomPrice;
    private double roomSize;
    private String roomDescription;
    private String roomDistrict;
    private RoomStatus roomStatus;
    private Date addDate;
    private UserDto user;
    private Set<CommentDto> comments = new HashSet<>();
}
