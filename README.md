Chức năng tiêu biểu cho thuê phòng trọ 
1 Đăng tin cho thuê: Người dùng có thể đăng thông tin về các phòng trọ, bao gồm ảnh, mô tả, giá cả, vị trí, tiện ích và các thông tin khác liên quan.
2. Tìm kiếm và lọc: Người dùng có thể tìm kiếm các phòng trọ dựa trên các tiêu chí như vị trí, diện tích, giá cả, tiện ích, và các yêu cầu khác. Họ cũng có thể áp dụng bộ lọc để thu hẹp kết quả tìm kiếm theo nhu cầu cụ thể.
3. Đánh giá và nhận xét: Người dùng có thể đánh giá và viết nhận xét về các phòng trọ đã thuê trước đó, giúp người dùng khác
4.Quản lý tài khoản: Người dùng có thể tạo tài khoản cá nhân, lưu lại các phòng trọ yêu thích, quản lý thông tin cá nhân và theo dõi lịch sử giao dịch.
5. Admin quản lý người dùng: Admin đăng nhập thành công chuyển đến trang quản lý người dùng. Trang này hiển thị danh sách các người dùng đăng ký trên hệ thống
Mô tả thứ tự diễn ra cho các chức năng
1.	Đăng tin cho thuê: 
Bước 1: Người dùng vào trang web cho thuê phòng trọ, thấy các mục điều hướng như đăng nhập/ đăng ký, tìm kiếm, đăng tin.
-	Người dùng muốn đăng tin thì phải login vào trang web, trong trường hợp chưa có tài khoản người dùng cần đăng ký mới bằng cách cung cấp thông tin như tên, địa chỉ , email, số điện thoại.
Bước 2: Người dùng click vào button “ Đăng tin”. Hiển thị lên cho người dùng là một form điền đầy đủ các thông tin về phòng trọ như hình ảnh, dịa chỉ, diện tích, giá cả , mô tả, 
-	Có thể có các trường bắt buộc phải điền, vị dụ như địa chỉ, và giá cả, trong khi các trường khác có thẻ tự chọn.
Bước 3: Xác nhận và gửi đăng tin , trước khi đăng tin người dùng có thể xem lại các thông tin đã điền và sửa thông tin nếu cần thiêt
-	Thông tin hợp lệ người dùng có thể nhân nút “ Gửi đăng tin “ để hoàn thành quá trình đăng tin.
Bước 4: Xử lý đăng tin
-	Hệ thống kiểm tra thông tin đăng tin và lưu vào cơ sở dữ liệu nếu thông tin hợp lệ.Hệ thống sẽ thông báo “ đăng tin thành công”
-	Nếu có lỗi trong quá trình xử lý, hệ thống thông báo cho người dùng về lỗi cụ thể như : (thiếu thông tin bắt buộc, hình ảnh quá lớn, …) và yêu cầu người dùng sửa lại.
2.	Tìm kiếm và lọc:
	Bước 1: Người dùng vào trang chủ chọn ô tìm kiếm với các tiêu chí như vị trí, diện tích, giá cả, … 
-	Sau đó người dùng nhập các thông tin tìm kiếm , người dùng nhấn nút “ Tìm Kiếm” để tiếp tục.
Bước 2: Hệ thống tìm kiếm và lọc các phòng trọ dựa trên các thông tin người dùng cung cấp và hiển thị các phòng trọ phù hợp.
-	Hiển thị các phòng trọ dưới dạng danh sách, với thông tin cơ bản của mỗi phòng trọ như vị trí , ảnh , giá cả.
Bước 3: Người dùng có thể quay lại kết quả tìm kiếm hoặc trang chủ để thực hiện tìm kiếm và lọc khác nếu cần thiết.
Ngoại lệ: Người dùng nhập vào các thông tin không phù hợp , hệ thống trả về thông báo “ không tìm thấy phòng trọ phù hợp”.
Xem chi tiết phòng trọ
	Bước 1: Người dùng vào trang chủ hoặc sau khi thực hiện kết quả tìm kiếm thì hệ thống hiển thị danh sách phòng trọ .
	Bước 2: Người dùng nhấp vào 1 bài đăng để biết về thông tin chi tiết về phòng trọ như hình ảnh, mô tả, tiện ích, vị trí, thông tin liên hệ với người cho thuê
-	Trên trang chi tiết phòng trọ, người dùng có thể lấy thông tin liên hệ người cho thuê như số diện thoại , gmail hoặc gửi tin nhắn trực tiếp 
3.	Đánh giá và nhận xét
	Bước 1: Người dùng chọn một phòng trọ cụ thể , và hiện lên thông tin của phòng trọ gồm ảnh , vị trí , diện tích , giá cả, …

	Bước 2: Hệ thống cung cấp một giao diện cho phép người dùng đánh giá và nhận xét về phòng trọ
-	Người dùng có thể chọn một số sao ( 1 đến 5 sao ) để đánh giá chất lượng phòng trọ 
-	Người dùng có thể viết nhận xét bổ sung để chia sẻ trải nghiệm và ý kiến của mình với người dùng khác.
Bước 3: Gửi kết quả nhận xét 
-	Người dùng sau khi nhận xét sẽ click vào button “ Gửi” .Hệ thống xử lý đánh giá và lưu trữ chúng trong cơ sở dữ liệu.
Ngoại lệ: Người dùng nhận xét với các từ ngữ không phù hợp. Hệ thống xử lý và yêu cầu nhập lại đối với người dùng. 
4.	Quản lý tài khoản
Bước 1: Sau khi login thành công , thì người dùng chuyển đến trang quản lý tài khoản
-	Trang quản lý tài khoản cung cấp các phần hiển thị thông tin các cá nhân : tên , số điện thoại, địa chỉ , email và các thông tin liên quan đến tài khoản
Bước 2: Sửa thông tin các nhân
-	Người dùng chọn sửa thông tin các nhân , tương tự như cập nhật thông tin cá nhân của người dùng như : tên, địa chỉ, số điện thoại, email, vv.
-	Người dùng sau khi chỉnh sửa xong nhấp “ cập nhật “ hệ thống xử lý và lưu lại thông tin người dùng.
Ngoại lệ: Người dùng sau khi chỉnh sửa thông tin và nhấp “ cập nhật” hệ thống thông báo thành công, nhưng thông tin chưa được chỉnh sửa luôn do hệ thống cần thời gian để xử lý.
Bước 3: Đổi mật khẩu:
-	Người dùng chọn thay đổi mật khẩu , hệ thống hiển thị lên nhập mật khẩu hiện tại , và nhập hai lần mật khẩu mới.
-	 Người dùng nhấp xác nhận hệ thống xử lý và thông báo thay đổi mật khẩu mới thành công.
Ngoại lệ: Người dùng nhập sai mật khẩu hiện tại, hệ thống sẽ thông báo người dùng “ mật khẩu hiện tại không đúng” và yêu cầu nhập lại.
5.	Admin quản lý người dùng
	Bước 1: Admin login thành công  chuyển đến trang quản lý người dùng.
-	Trang này hiển thị danh sách người dùng đã đăng ký và thông tin người dùng bao gồm tên , địa chỉ , số điện thoại, email,
-	Admin có thể xem chi tiết người dùng bằng cách nhấp vào người dùng đó.
Ngoại lệ: Admin nhấp vào người dùng mà không tồn tại , hệ thống hiển thị thông báo lỗi , và không hiển thị thông tin người dùng.
Bước 2: Admin sửa thông tin người dùng
-	Admin nhấp vào nút “ chỉnh sửa” để sửa thông tin người dùng như cập nhật thông tin với các thông tin như tên , địa chỉ , số điện thoại, email.
Chỉnh sửa:Admin chỉnh sửa các trường thông tin không đúng định dạng, hệ thống yêu cầu kiểm tra lại thông tin và yêu cầu chỉnh sửa đúng
Bước 3: Xóa người dùng
	Admin xóa người dùng khỏi hệ thống .Hệ thống cần xác nhận từ Admin trước khi thực hiện xóa người dùng 
Sau khi hệ thống xác nhận, hệ thống sẽ xóa thông tin người dùng và loại bỏ tài khoản của ng
