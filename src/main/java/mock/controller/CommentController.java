package com.mock.controller;

import com.mock.entity.User;
import com.mock.exception.UnauthorizedAccessException;
import com.mock.payload.reponse.ApiResponse;
import com.mock.payload.request.CommentDto;
import com.mock.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/comments")
@CrossOrigin(origins = "*")
public class CommentController {
    @Autowired
    private CommentService commentService;
    @PostMapping("/post/{postId}")
    public ResponseEntity<CommentDto> createComment(@RequestBody CommentDto comment, @PathVariable Integer postId){
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(user.isStatus()) {
        CommentDto createComment =  this.commentService.createComment(comment,postId);
        return new ResponseEntity<CommentDto>(createComment, HttpStatus.CREATED);
        }else {
            throw new UnauthorizedAccessException("Only enabled ROLE_POSTER can comment!");
        }
    }
    @DeleteMapping("/{commentId}")
    public ResponseEntity<ApiResponse> deleteComment(@PathVariable Integer commentId){
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(user.isStatus()) {
        this.commentService.deleteComment(commentId);
        return new ResponseEntity<ApiResponse>(new ApiResponse("Comment deleted sucsessfully!",true), HttpStatus.OK);
        }else {
            throw new UnauthorizedAccessException("Only enabled ROLE_POSTER can delete comment!");
        }
    }
}
