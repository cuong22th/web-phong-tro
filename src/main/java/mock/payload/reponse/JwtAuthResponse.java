package com.mock.payload.reponse;

import lombok.Data;

@Data
public class JwtAuthResponse {
    private String token;
}
