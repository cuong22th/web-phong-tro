package com.mock.entity;

public enum ERole {
    ROLE_ADMIN, ROLE_POSTER, ROLE_USER
}
