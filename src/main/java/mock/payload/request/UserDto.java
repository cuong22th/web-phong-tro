package com.mock.payload.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@Getter
@Setter
public class UserDto {
    private int id;
    @NotEmpty
    @Size(min = 4, max = 24, message = "Username must be of 6-24 characters!")
    private String username;
    @NotEmpty
    @Size(min = 2, max = 10, message = "FirstName must be of 6-24 characters!")
    private String firstName;
    @NotEmpty
    @Size(min = 2, max = 10, message = "LastName must be of 6-24 characters!")
    @Column(name = "last_name",nullable = false,length = 50)
    private String lastName;
    @Email(message = "Email address is not valid")
    private String email;
    private String password;
    @NotEmpty
    @Size(min=9, max = 11, message = "PhoneNumber must be of 9-11 characters!" )
    private String phone;
    private String avatar="default.png";
    private boolean status=true;

    private Set<RoleDto> roles = new HashSet<>();

}
